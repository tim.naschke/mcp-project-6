using LinearAlgebra
using Printf
include("model.jl")


function eigvec_LaTeX(vec)
    res = "\$("

    for num in vec
        if abs(num) <= 1e-10
            res *= "0, "
        else
            res *= "\\SI{"
            res *= @sprintf "%.2e" num
            res *= "}{}, "
        end
    end

    res *= ")^\\top\$"
    return res
end


spins_eval = 2:6

for i in spins_eval
    H = Hamiltonian(i)
    
    eigval, eigvec = eigen(H)
    ground_index = argmin(eigval)
    
    println()
    println("number of spins: $i")
    println("ground state energy: $(eigval[ground_index])")
    println("ground state:")
    display(eigvec[:, ground_index])
    println(eigvec_LaTeX(eigvec[:, ground_index]))
    display("m_z: $(m_z(eigvec[:, ground_index]))")
end
