using LinearAlgebra
using LaTeXStrings
using Plots
include("model.jl")


function run()
    spins = 10
    m = 0
    H = Hamiltonian(spins)

    _, btransform = block_basis(spins, m)
    Hₘ = H[btransform, btransform]
    M = length(btransform)

    λ = zeros(M, 4)

    for Λ in 1:M
        Hₗ = Matrix(lanczos_basis(Hₘ, Λ))
        λ[Λ, 1:min(Λ, 4)] = eigvals(Hₗ)[1:min(Λ, 4)]
    end

    return λ
end


@time λ = run()


plt = plot(
    xlabel = L"\Lambda",
    ylabel = "energy",
    dpi = 600
)
plot!(λ[:, 1], label=L"E_0")
plot!(λ[:, 2], label=L"E_1")
plot!(λ[:, 3], label=L"E_2")
plot!(λ[:, 4], label=L"E_3")

savefig("plots/4b.png")
display(plt)
