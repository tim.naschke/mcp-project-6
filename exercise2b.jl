using LinearAlgebra
using Statistics
using Plots
using ProgressMeter
using LaTeXStrings
using LsqFit
include("model.jl")


function run(spins_eval)
    times = []

    for i in spins_eval
        H = Hamiltonian(i)

        time = @elapsed eigen(H)

        push!(times, time)
    end

    return times
end


spins_eval = 2:12
times = run(spins_eval)


plt1 = scatter(
    spins_eval,
    times,
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600
)

model(x, p) = p[1] * exp.(p[2] * x)
fit = curve_fit(model, spins_eval, times, [1., 2.])
x_fit = LinRange(spins_eval[1], spins_eval[end], 100)

display(fit.param)

plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)

plt2 = scatter(
    spins_eval,
    times,
    yaxis = :log,
    yticks = 10. .^(-10:10),
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600,
    legend_position = :topleft
)
plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)


plt = plot(plt1, plt2, layout=2)

savefig("plots/2b.png")
display(plt)
