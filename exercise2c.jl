using ProgressMeter
using LinearAlgebra
using LaTeXStrings
using Plots
include("model.jl")


N_eval = 4:12


function average(spins)
    basis = construct_basis(spins)
    N = 2^spins
    H = Hamiltonian(spins)
    λ, v = eigen(H)
    
    corr = 0
    m = 0
    m² = 0
    E = 0
    E² = 0

    Z = sum(exp.(-λ))

    for i in 1:N, j in 1:N
        corr += v[j, i] * correlation(basis[j]) * exp(-λ[i])
        m += v[j, i] * m_z(basis[j]) * exp(-λ[i])
        #m² += v[j, i] * m_z(basis[j])^2 * exp(-λ[i])
    end

    for i in 1:N
        E += λ[i] * exp(-λ[i])
        E² += λ[i]^2 * exp(-λ[i])
        m² += m_z(v[:, i])^2 * exp(-λ[i])
    end

    corr /= Z
    m /= Z
    m² /= Z
    E /= Z
    E² /= Z

    χ = (m² - m^2)
    C = (E² - E^2)

    return corr, χ, C
end


function run()
    len = length(N_eval)

    correlator = zeros(Float64, len)
    χ = zeros(Float64, len)
    C = zeros(Float64, len)

    progress = Progress(len)
    Threads.@threads for i in 1:len
        res = average(N_eval[i])

        correlator[i] = res[1]
        χ[i] = res[2]
        C[i] = res[3]

        next!(progress)
    end

    return correlator, χ, C
end


res = run()


plt = plot(
    N_eval,
    res[1],
    dpi = 600,
    xlabel = L"N",
    ylabel = L"\langle S_{z,0} S_{z,2} \rangle",
    label = "numerical"
)

savefig("plots/2c_corr.png")
display(plt)

plt = plot(
    N_eval,
    res[2],
    dpi = 600,
    xlabel = L"N",
    ylabel = L"χ",
    label = "numerical"
)
hline!([1/4], color="black", label="high-temperature limit")

savefig("plots/2c_susc.png")
display(plt)

plt = plot(
    N_eval,
    res[3],
    dpi = 600,
    xlabel = L"N",
    ylabel = L"C",
    label = "numerical"
)
hline!([3/13], color="black", label="high-temperature limit")

savefig("plots/2c_heat.png")
display(plt)
