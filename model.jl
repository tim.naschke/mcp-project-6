using LinearAlgebra
using Distributed
using BitOperations

function ∘(a::BitVector, b::BitVector)
    return a == b ? 1 : 0
end

function ∘(a::BitVector, b::Integer)
    if b != 0 && b != 1
        throw(DomainError(b, "Multiplictation of a BitVector with an Integer is only allowed for 0 and 1"))
    end

    if b == 0
        return 0
    end

    return a
end

function ∘(a::Integer, b::BitVector)
    return ∘(b, a)
end


function Sᶻ(state::BitVector, index)
    i = mod1(index, length(state))

    return state[i] - 1/2
end


function S⁺(state::BitVector, index)
    i = mod1(index, length(state))

    if state[i] == 1
        return 0
    end
    
    new_state = copy(state)
    new_state[i] = 1
    
    return new_state
end

function S⁺(state::Integer, index)
    return 0
end


function S⁻(state::BitVector, index)
    i = mod1(index, length(state))

    if state[i] == 0
        return 0
    end
    
    new_state = copy(state)
    new_state[i] = 0
    
    return new_state
end

function S⁻(state::Integer, index)
    return 0
end


function construct_basis(spins)
    basis = Vector{BitVector}(undef, 0)
    N = 2^spins

    for i in 0:N-1
        vec = BitVector(digits(
            i,
            base = 2,
            pad = spins
        ))

        push!(basis, vec)
    end

    return basis
end


function Hamiltonian(N)
    H = zeros(2^N,2^N)

    for a in 0:2^N-1
        for i in 0:N-1
            j = mod(i+1,N)
            if bget(a,i) == bget(a,j)
                H[a+1,a+1] += 1/4
            else
                H[a+1,a+1] += -1/4
                b = bflip(a, i)
                b = bflip(b, j)
                H[a+1, b+1] += 1/2
            end 
        end
    end

    return H
end


function Hamiltonian_old(spins)
    N = 2^spins
    basis = construct_basis(spins)
    H = zeros(N, N)

    for row in 1:N, column in 1:N
        for i in 1:spins
            H[row, column] += Sᶻ(basis[column], i) * Sᶻ(basis[column], i+1) * (basis[row] ∘ basis[column])
            H[row, column] += 1/2 * (basis[row] ∘ S⁺(S⁻(basis[column], i+1), i))
            H[row, column] += 1/2 * (basis[row] ∘ S⁻(S⁺(basis[column], i+1), i))
        end
    end

    return H
end


function Hamiltonian_dist(spins)
    N = 2^spins

    H_total = @distributed (+) for row in 1:N
        H = zeros(N, N)
        basis = construct_basis(spins)
        
        for column in 1:N
            for i in 1:spins
                H[row, column] += Sᶻ(basis[column], i) * Sᶻ(basis[column], i+1) * (basis[row] ∘ basis[column])
                H[row, column] += 1/2 * (basis[row] ∘ S⁺(S⁻(basis[column], i+1), i))
                H[row, column] += 1/2 * (basis[row] ∘ S⁻(S⁺(basis[column], i+1), i))
            end
        end

        H
    end

    return fetch(H_total)
end


function m_z(state::BitVector)
    res = 0

    for bit in state
        res += bit - 1/2
    end

    return res
end

function m_z(vector::Vector{Float64})
    basis = construct_basis(Int(log2(length(vector))))      # ToDo: check if length 2^x

    return sum(vector .* m_z.(basis))
end


function correlation(state::BitVector)
    return 3 * Sᶻ(state, 0) * Sᶻ(state, 2)
end


function χ(spins, T)
    m_z²(state::BitVector) = m_z(state)^2
    return thermal_average(m_z², spins, T) - thermal_average(m_z, spins, T)^2
end


function thermal_average(O::Function, spins, T, args::Tuple=())
    """
    Thermal average of Observable O of form O(state::BitVector, args...)
    """
    basis = construct_basis(spins)
    N = 2^spins
    H = Hamiltonian(spins)
    E = eigvals(H)
    v = eigvecs(H)

    res = 0

    for i in 1:N, j in 1:N
        res += v[j, i] * O(basis[j], args...) * exp(-E[i] / T)
    end

    return res / sum(exp.(-E / T))
end


function block_basis(spins, m)
    basis = construct_basis(spins)

    bbasis = Vector{BitVector}(undef, 0)
    btransform = Vector{Integer}(undef, 0)

    for i in 1:2^spins
        if m_z(basis[i]) == m
            push!(bbasis, basis[i])
            push!(btransform, i)
        end
    end

    return bbasis, btransform
end


function lanczos_basis(H::Matrix{Float64}, Λ)
    M = LinearAlgebra.checksquare(H)
    f = Vector{Vector{Float64}}(undef, Λ)

    @assert Λ <= M

    if Λ == 0
        return zeros(0, 0)
    end

    if M == 1
        return H
    end

    f[1] = rand(M)

    if Λ >= 2
        f[2] = H * f[1] - (f[1]' * H * f[1]) / norm(f[1])^2 * f[1]
    end

    for i in 3:Λ
        a = (f[i-1]' * H * f[i-1]) / norm(f[i-1])^2
        b = norm(f[i-1])^2 / norm(f[i-2])^2
        f[i] = H * f[i-1] - a * f[i-1] - b * f[i-2]
    end

    """
    for i in 1:Λ
        f[i] = f[i] / norm(f[i])
    end

    for i in 2:Λ
        for j in 1:i-1
            q = f[j] ⋅ f[i]
            f[i] = (f[i] - q * f[j]) / (1 - q^2)
        end
    end
    """

    dl = @. √(norm(f[2:end])^2 / norm(f[1:end-1])^2)
    d = [(f[i]' * H * f[i]) / norm(f[i])^2 for i in 1:Λ]
    du = @. √(norm(f[2:end])^2 / norm(f[1:end-1])^2)
    
    return Tridiagonal(dl, d, du)
end
