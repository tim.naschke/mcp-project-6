using LinearAlgebra
using LaTeXStrings
using Plots
using LsqFit
include("model.jl")


function run(spins)
    H = Hamiltonian(spins)
    E₀ = Inf

    for i in 1:spins+1
        m = (-spins/2:spins/2)[i]
        _, btransform = block_basis(spins, m)

        Hₘ = H[btransform, btransform]

        Λ = 1
        M = length(btransform)
        ε₀ = Inf
        gs_prev = Inf

        while Λ <= M && abs(ε₀) > 1e-6
            Hₗ = Matrix(lanczos_basis(Hₘ, Λ))
            gs = eigvals(Hₗ)[1]

            E₀ = min(E₀, gs)
            ε₀ = abs(gs - gs_prev)
            gs_prev = gs

            Λ += 1
        end
    end

    return E₀
end


function measure(spins_eval)
    return [@elapsed run(spins) for spins in spins_eval]
end


spins_eval = 2:15
times = measure(spins_eval)


plt1 = scatter(
    spins_eval,
    times,
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600
)

model(x, p) = p[1] * exp.(p[2] * x)
fit = curve_fit(model, spins_eval, times, [1., 2.])
x_fit = LinRange(spins_eval[1], spins_eval[end], 100)

display(fit.param)

plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)

plt2 = scatter(
    spins_eval,
    times,
    yaxis = :log,
    yticks = 10. .^(-10:10),
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600,
    legend_position = :topleft
)
plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)


plt = plot(plt1, plt2, layout=2)

savefig("plots/4a.png")
display(plt)
