using LinearAlgebra
using Plots
using LaTeXStrings
using ProgressMeter
using LsqFit
include("model.jl")


function measure(spins)
    H = Hamiltonian(spins)
    t = 0

    for m in -spins/2:spins/2
        _, btransform = block_basis(spins, m)

        Hₘ = H[btransform, btransform]

        t += @elapsed eigen(Hₘ)
    end

    return t
end


function run()
    times = []

    @showprogress for spins in spins_eval
        t = measure(spins)
        push!(times, t)
    end

    return times
end


spins_eval = 2:14
times = run()


plt1 = scatter(
    spins_eval,
    times,
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600
)

model(x, p) = p[1] * exp.(p[2] * x)
fit = curve_fit(model, spins_eval, times, [1., 2.])
x_fit = LinRange(spins_eval[1], spins_eval[end], 100)

display(fit.param)

plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)

plt2 = scatter(
    spins_eval,
    times,
    yaxis = :log,
    yticks = 10. .^(-10:10),
    xlabel = L"length of spin chain $N$",
    ylabel = L"$t$ [s]",
    label = "mean execution time",
    dpi = 600,
    legend_position = :topleft
)
plot!(
    x_fit,
    model(x_fit, fit.param),
    label = "exponential fit"
)


plt = plot(plt1, plt2, layout=2)

savefig("plots/3.png")
display(plt)
