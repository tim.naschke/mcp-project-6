using Distributed

cip_pcs = ["c060", "c063", "c065", "c071"]
machines = [("$(pc).physikcip.uni-goettingen.de", :auto) for pc in cip_pcs]

if nworkers() <= 1
    addprocs(machines)
end
