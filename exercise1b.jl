function period(state_int::Integer)
    state = digits(state_int, base=2)

    for i in 1:length(state)
        test = circshift(state, i)
        test_reverse = circshift(state, -i)

        if test == state || test_reverse == state
            return i
        end
    end
end


a = 9588514242
#a = 1234567891034987523940527

display(period(a))
